//! Distributed cards implements the card shuffling algorithm described in the [Mental Poker
//! Wikipedia page](https://en.wikipedia.org/wiki/Mental_poker) for n cards.
//!
//! The crate resolves around the [Deck] struct.
//!
//! To serialize/deserialize Primes and Rsa keys, they have be converted into the Unchecked variants.
//!
//! # Example usage
//!
//! ```
//! use distributed_cards::{Deck, Prime};
//! use rand::prelude::*;
//!
//! let mut rng = thread_rng();
//!
//! // can be reused in each game list of primes from all players has to be collected before the
//! // shuffling can start
//! let p = Prime::random(512, &mut rng);
//!
//! ```

mod crypto;
mod deck;

pub use crypto::{Prime, Rsa, RsaParameter, UncheckedPrime, UncheckedRsa};
pub use deck::{Deck, EncryptedDeck};
pub use num_bigint_dig::BigUint;
